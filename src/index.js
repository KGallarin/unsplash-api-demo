import React from "react";
import ReactDOM from "react-dom";
import "./styles/App.css";
import Provider from "react-redux";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";
import Store from "./store";

let store = Store();
window.store = store;

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
