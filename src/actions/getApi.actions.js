export const REQUEST_IMAGES = "REQUEST_IMAGES";
export const RECIEVE_IMAGES = "RECIEVE_IMAGES";
export const INPUT_SEARCH = "INPUT_SEARCH";
export const INVALIDATE_IMAGES = "INVALIDATE_IMAGES";

export const inputSearch = image => ({
  type: INPUT_SEARCH,
  image
});

export const invalidateImages = image => ({
  type: INVALIDATE_IMAGES,
  image
});

export const recieveImages = (imageQuery, json) => ({
  type: RECIEVE_IMAGES,
  imageQuery,
  images: json.results.map(response => response),
  receivedAt: Date.now()
});

export const requestImages = image => ({
  type: REQUEST_IMAGES,
  image
});

const fetchImages = imageQuery => dispatch => {
  dispatch(requestImages(imageQuery));
  return fetch(
    `https://api.unsplash.com/search/photos/?page=1&per_page=10&query=${imageQuery}&client_id=${
      process.env.REACT_APP_ACESS_KEY
    }`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    }
  )
    .then(response => response.json())
    .then(json => dispatch(recieveImages(imageQuery, json)));
};

const shouldFetchImages = (state, image) => {
  const images = state.imagesByApi[image];
  if (!images) {
    return true;
  }
  if (images.isFetching) {
    return false;
  }
  return images.didInvalidate;
};

export const fetchImagesIfNeeded = image => (dispatch, getState) => {
  if (shouldFetchImages(getState(), image)) {
    return dispatch(fetchImages(image));
  }
};
