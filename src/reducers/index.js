import { combineReducers } from "redux";
import {
  INPUT_SEARCH,
  INVALIDATE_IMAGES,
  REQUEST_IMAGES,
  RECIEVE_IMAGES
} from "../actions/getApi.actions";

const queryValue = (state = "dark", action) => {
  switch (action.type) {
    case INPUT_SEARCH:
      return action.image;
    default:
      return state;
  }
};

const images = (
  state = { isFetching: false, didInvalidate: false, dataItems: [] },
  action
) => {
  switch (action.type) {
    case INVALIDATE_IMAGES:
      return {
        ...state,
        didInvalidate: true
      };
    case REQUEST_IMAGES:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      };
    case RECIEVE_IMAGES:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        dataItems: action.images,
        lastUpdated: action.receivedAt
      };
    default:
      return state;
  }
};

const imagesByApi = (state = {}, action) => {
  switch (action.type) {
    case INVALIDATE_IMAGES:
    case RECIEVE_IMAGES:
    case REQUEST_IMAGES:
      return {
        ...state,
        [action.imageQuery]: images(state[action.imageQuery], action)
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  imagesByApi,
  queryValue
});

export default rootReducer;
