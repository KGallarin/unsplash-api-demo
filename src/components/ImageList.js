import React from "react";

const ImageList = ({ images }) => (
  <ul>
    {images.map((images, i) => (
      <li key={i}>
        <img src={images.urls.small} alt="/" />
      </li>
    ))}
  </ul>
);

export default ImageList;
