import React from "react";

const Header = () => (
  <header className="App-header">
    {/* <img src={logo} className="App-logo" alt="logo" /> */}
    <h1 className="App-title">Unsplash Sample </h1>
  </header>
);

export default Header;
