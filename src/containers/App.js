import React, { Component } from "react";
import { connect } from "react-redux";
import ImageList from "../components/ImageList";
import {
  invalidateImages,
  inputSearch,
  fetchImagesIfNeeded
} from "../actions/getApi.actions";

class App extends Component {
  componentDidMount() {
    const { dispatch, queryValue } = this.props;
    dispatch(fetchImagesIfNeeded(queryValue));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.queryValue !== this.props.queryValue) {
      const { dispatch, queryValue } = nextProps;
      dispatch(fetchImagesIfNeeded(queryValue));
    }
  }
  handleChange = nextInputSearch => {
    const description = nextInputSearch.target.value;
    this.props.dispatch(inputSearch(description));
  };

  handleRefresh = e => {
    e.preventDefault();

    const { dispatch, queryValue } = this.props;
    dispatch(invalidateImages(queryValue));
    dispatch(fetchImagesIfNeeded(queryValue));
  };

  render() {
    const { images, isFetching, lastUpdated } = this.props;
    const isEmpty = images.length === 0;
    console.log("imagesko", images);
    return (
      <div>
        <input type="text" placeholder="filter" onChange={this.handleChange} />
        <p>
          {lastUpdated && (
            <span>
              Last updated at {new Date(lastUpdated).toLocaleDateString()}
            </span>
          )}
          {!isFetching && (
            <button type="button" onClick={this.handleRefresh}>
              {" "}
              Refresh{" "}
            </button>
          )}
        </p>
        {isEmpty ? (
          isFetching ? (
            <h2> Loading... </h2>
          ) : (
            <h2>Empty</h2>
          )
        ) : (
          <div style={{ opacity: isFetching ? 0.4 : 1 }}>
            <ImageList images={images} />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { queryValue, imagesByApi } = state;
  const { isFetching, lastUpdated, dataItems: images } = imagesByApi[
    queryValue
  ] || { isFetching: true, dataItems: [] };

  return {
    queryValue,
    images,
    isFetching,
    lastUpdated
  };
};

export default connect(mapStateToProps)(App);
